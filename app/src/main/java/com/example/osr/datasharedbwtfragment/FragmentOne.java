package com.example.osr.datasharedbwtfragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class FragmentOne extends Fragment {
    EditText firstName,lastName;
    Button submitBtn;

    public FragmentOne() {
        // Required empty public constructor
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_fragment_one,container,false);
        // Inflate the layout for this fragment
        firstName=(EditText)view.findViewById(R.id.firstname);
        lastName=(EditText)view.findViewById(R.id.lastname);
        submitBtn=(Button)view.findViewById(R.id.submitBtn);
        //return inflater.inflate(R.layout.fragment_fragment_one, container, false);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String first=firstName.getText().toString();String last=lastName.getText().toString();
            Bundle bundal=new Bundle();bundal.putString("FirstName",first);bundal.putString("LastName",last);
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
               FragmentTwo fragmentTwo=new FragmentTwo();
              fragmentTwo.setArguments(bundal);
             fragmentTransaction.replace(R.id.frame,fragmentTwo);
             fragmentTransaction.commit();
        }
});
      return  view;
}
}