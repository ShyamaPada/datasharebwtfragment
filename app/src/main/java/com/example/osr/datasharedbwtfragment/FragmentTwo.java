package com.example.osr.datasharedbwtfragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



public class FragmentTwo extends Fragment {


    public FragmentTwo() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_fragment_two, container, false);
        View view=inflater.inflate(R.layout.fragment_fragment_two,container,false);
        Bundle bundle=getArguments();
        String firstName=bundle.getString("FirstName");
        String lastName=bundle.getString("LastName");
        TextView firstText=view.findViewById(R.id.firstname);
        TextView lastText=view.findViewById(R.id.lastname);
        firstText.setText(firstName);
        lastText.setText(lastName);
        return  view;
    }


}
